<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class jabatan_model extends CI_Model
{
	//panggil nama table
	private $_table = "jabatan";
	
	public function tampilDataJabatan()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	}

	public function tampilDataJabatan2()
	{
		//menggunakan query
		$query = $this->db->query("SELECT * FROM jabatan where flag = 1");
		return $query->result();
	}

	public function tampilDataJabatan3()
	{
		//menggunakan query 
		$this->db->select('*');
		$this->db->order_by('kode_jabatan', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function save()
	{
		$data['kode_jabatan']					= $this->input->post('kode_jabatan');
		$data['nama_jabatan']			= $this->input->post('nama_jabatan');
		$data['keterangan']			= $this->input->post('keterangan');
		$data['flag']					= 1;
		$this->db->insert($this->_table, $data);
	
	}
	public function update($kode_jabatan)
	{
		$data['kode_jabatan']					= $this->input->post('kode_jabatan');
		$data['nama_jabatan']			= $this->input->post('nama_jabatan');
		$data['keterangan']			= $this->input->post('keterangan');
		$data['flag']					= 1;
		$this->db->where('kode_jabatan', $kode_jabatan);
		$this->db->update($this->_table, $data);
	
	}
	public function detail($kode_jabatan)
	{
		$this->db->select('*');
		$this->db->where('kode_jabatan', $kode_jabatan);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();		
	}
	public function edit_jabatan($kode_jabatan)
	{
		$this->db->select('*');
		$this->db->where('kode_jabatan', $kode_jabatan);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();		
	}

	
}