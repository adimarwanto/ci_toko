<table width="100%" border="1" >
  <tr>
    <td align="center" bgcolor="#0000CC"><font color="#FFFFFF">&quot;Toko Jaya Abadi&quot;</font></td>
  </tr>
  <tr>
    <td bgcolor="#000099"><table width="580" border="1" align="center" bgcolor="#CCFFFF">
      <tr align="center">
        <td width="55" height="42"><a href="">Home</a></td>
        <td width="93"><a href="<?=base_url();?>karyawan/listkaryawan">Data Karyawan</a></td>
        <td width="73"><a href="<?=base_url();?>jabatan/listjabatan">Data Jabatan</a></td>
        <td width="71"><a href="<?=base_url();?>barang/listbarang">Data Barang</a></td>
        <td width="76"><a href="<?=base_url();?>supplier/listsupplier">Data Supplier</a></td>
        <td width="74"><a href="<?=base_url();?>jenis_barang/listjenis">Jenis Barang</a></td>
      </tr>
    </table></td>
  </tr>
  <tr cellspacing="">
    <td><table width="100%" border="0" cellspacing="">
      <tr>
        <td align="center"><table width="100%" border="0">
        </table></td>
        </tr>
      <tr>
        <td><table width="100%" border="0">
        </table>
          <table width="100%" border="1">
            <tr>
              <td colspan="6"><table width="1094" border="0">
                <tr align="center" >
                  <td width="1084">
<form name="form1" method="post" action="<?=base_url();?>karyawan/inputkaryawan">
<div align="center"><h1>Input Data Karyawan</h1></div>
<table width="50%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#00FF00">
  <tr>
    <td width="42%">NIK</td>
    <td width="3%"> :</td>
    <td width="55%">
      <input type="text" name="nik" id="nik" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Nama</td>
    <td>:</td>
    <td>
      <input type="text" name="nama_karyawan" id="nama_karyawan" maxlength="50">
    </td>
  </tr>
    <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td>
      <input type="text" name="tempat_lahir" id="tempat_lahir" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Jenis Kelamin</td>
    <td>:</td>
    <td>
      <select name="jenis_kelamin" id="jenis_kelamin">
      	<option value="P">Perempuan</option>
        <option value="L">Laki-Laki</option>
      </select>
   </td>
  </tr>
  
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td>
      <select name="tgl" id="tgl">
      <?php
      	for($tgl=1;$tgl<=31;$tgl++){
      ?>
      	<option value="<?=$tgl;?>"><?=$tgl;?></option>
       <?php
       	}
       ?>
      </select>
      
      <select name="bln" id="bln">
      <?php
      	$bulan_n = array('Januari','Februari','Maret','April',
        				'Mei','Juni','Juli','Agustus','September',
                        'Oktober','November','Desember');
         for($bln=0;$bln<12;$bln++){
      ?>
      	<option value="<?=$bln+1;?>">
        	<?=$bulan_n[$bln];?>
        </option>
        <?php
        	}
        ?>
      </select>
      
      <select name="thn" id="thn">
      <?php
      	for($thn = date('Y')-60;$thn<=date('Y')-15;$thn++){
      ?>
      	<option value="<?=$thn;?>"><?=$thn;?></option>
      <?php
      	} 
      ?>
      </select>
    </td>
  </tr>
      <tr>
    <td>Telepon</td>
    <td>:</td>
    <td>
      <input type="text" name="telp" id="telp" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td>
      <textarea name="alamat" id="alamat" cols="45" rows="5"></textarea>
   </td>
  </tr>
    <tr>
    <td>Jabatan</td>
    <td>:</td>
    <td>
      <select name="kode_jabatan" id="kode_jabatan">
      	<?php foreach ($data_jabatan as $data){ ?>
        <option value="<?=$data->kode_jabatan;?>"><?=$data->nama_jabatan; ?></option>
		<?php }?>
      </select>
   </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
      <input type="submit" name="Submit" id="Submit" value="Simpan">
      <input type="reset" name="reset" id="reset" value="Reset">
    </td>
  </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
            <a href="<?=base_url();?>karyawan/listkaryawan"><input type="button" name="kembali" id="kembali" value="Kembali ke Menu Sebelumnya"></a>
    </td>
  </tr>
</table>
</form>

                  </table></td>
                </tr>
    </table></td>
  </tr>
</table>
