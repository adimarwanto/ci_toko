<?php
defined('BASEPATH') OR Exit('No direct script access allowed');

class barang extends CI_Controller
{
	Public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("barang_model");
		$this->load->model("jenis_model");
	}	
	public function index()
	{
		$this->listbarang();
	}
	public function listbarang()
	{
		$data['data_jenis'] = $this->jenis_model->tampilDataJenis();
		$data['data_barang'] = $this->barang_model->tampilDataBarang2();
		$this->load->view('brg', $data); 	 
	}
	public function inputBarang()
	{
		$data['data_jenis'] = $this->jenis_model->tampilDataJenis();
		if(!empty($_REQUEST)){
			$m_barang = $this->barang_model;
			$m_barang->save();
			redirect("barang/index", "refresh");
			}
		$this->load->view('input_barang',$data); 	
		
	}
	public function detailbarang($kode_barang)
	{
				$data['data_jenis'] = $this->jenis_model->tampilDataJenis();
				$data['detail_barang'] = $this->barang_model->detail($kode_barang);
		$this->load->view('detail_b', $data); 	
	}
		public function edit_b($kode_barang)
	{
		
		$data['data_jenis'] = $this->jenis_model->tampilDataJenis();
		$data['detail_barang'] = $this->barang_model->editdetail($kode_barang);
		if(!empty($_REQUEST)){
			$m_barang = $this->barang_model;
			$m_barang->update($kode_barang);
			redirect("barang/index", "refresh");
			}
		$this->load->view('edit_barang',$data);
	}
}
	