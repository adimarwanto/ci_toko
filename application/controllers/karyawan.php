<?php
defined('BASEPATH') OR Exit('No direct script access allowed');

class karyawan extends CI_Controller
{
	Public function __construct()
	{
		parent::__construct();
		//load model terkait (untuk manggil scrip pertama kali di jalankan )
		$this->load->model("karyawan_model");
		$this->load->model("jabatan_model");
	}	
	public function index()
	{
		$this->listkaryawan();
	}
	 public function listkaryawan()
	{
		$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		$this->load->view('home', $data); 
	}
	public function inputKaryawan()
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		if(!empty($_REQUEST)){
			$m_karyawan = $this->karyawan_model;
			$m_karyawan->save();
			redirect("karyawan/index", "refresh");
			}
		$this->load->view('input_karyawan',$data); 	
	}
	public function listdetailkaryawan($nik)
	{
				$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
		$this->load->view('detail_k', $data); 	
	}
	public function edit($nik)
	{
				$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
				$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
			if(!empty($_REQUEST)){
			$m_karyawan = $this->karyawan_model;
			$m_karyawan->update($nik);
			redirect("karyawan/index", "refresh");
			}
		$this->load->view('edit_karyawan', $data); 	
	}
}