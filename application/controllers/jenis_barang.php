<?php
defined('BASEPATH') OR Exit('No direct script access allowed');

class jenis_barang extends CI_Controller
{
	Public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("jenis_model");
	}	
	public function index()
	{
		$this->listjenis();
	}
	 public function listjenis()
	{
	$data['data_jenis'] = $this->jenis_model->tampilDataJenis();
		$this->load->view('jns', $data); 
	}
	public function inputjenis()
	{
		$data['data_jenis'] = $this->jenis_model->tampilDataJenis();
		if(!empty($_REQUEST)){
			$m_jenis = $this->jenis_model;
			$m_jenis->save();
			redirect("jenis_barang/index", "refresh");
			}
		$this->load->view('input_jenis',$data); 	
	}
	public function detail_jenis($kode_jenis)
	{
				$data['detail_jenis'] = $this->jenis_model->detail($kode_jenis);
		$this->load->view('detail_jenis', $data); 	
	}
	public function edit_j($kode_jenis)
	{
		$data['detail_jenis'] = $this->jenis_model->edit_detail($kode_jenis);
		if(!empty($_REQUEST)){
			$m_jenis = $this->jenis_model;
			$m_jenis->update($kode_jenis);
			redirect("jenis_barang/index", "refresh");
			}
		$this->load->view('edit_jenis',$data); 	
	}
}