<?php
defined('BASEPATH') OR Exit('No direct script access allowed');

class supplier extends CI_Controller
{
	Public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("supplier_model");
	}	
	public function index()
	{
		$this->listsupplier();
	}
	 public function listsupplier()
	{
		 		$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		$this->load->view('splr', $data); 
	}
	public function inputSupplier()
	{
		$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		if(!empty($_REQUEST)){
			$m_supplier = $this->supplier_model;
			$m_supplier->save();
			redirect("supplier/index", "refresh");
			}
		$this->load->view('input_supplier',$data); 	
	}
	public function detailsupplier($kode_supplier)
	{
				$data['detail_supplier'] = $this->supplier_model->detail($kode_supplier);
		$this->load->view('detail_s', $data); 	
	}
	public function edit_s($kode_supplier)
	{
		$data['detail_supplier'] = $this->supplier_model->edit_detail($kode_supplier);
		if(!empty($_REQUEST)){
			$m_supplier = $this->supplier_model;
			$m_supplier->update($kode_supplier);
			redirect("supplier/index", "refresh");
			}
		$this->load->view('edit_supplier',$data); 	
	}
}